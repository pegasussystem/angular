import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {Test1Component} from './test/test1/test1.component';
import {Test2Component} from './test/test2/test2.component';
import {PrivilegeComponent} from './modules/privilege/privilege.component';
import {PrivilegeFormComponent} from "./modules/privilege/privilege-form/privilege-form.component";
import {MenuComponent} from "./modules/menu/menu.component";


const routes: Routes = [

  { path: 'test', component: Test1Component, children: [
      { path: '1', component: Test1Component },
      { path: '2', component: Test2Component },
    ] },

  { path: 'test-1', component: Test1Component},
  { path: 'test-2', component: Test2Component},



  { path: 'privilege', component: PrivilegeComponent},
  { path: 'privilege/create', component: PrivilegeFormComponent },

  { path: 'menu', component: MenuComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
