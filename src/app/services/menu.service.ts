import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Subject} from 'rxjs';
import {MenuModel} from '../models/menu.model';
import {map} from 'rxjs/operators';
import CustomStore from 'devextreme/data/custom_store';
import { Constant } from '../shared/constant';

@Injectable({providedIn: 'root'})
export class MenuService {

  menus = new Subject<MenuModel[]>();

  constructor(private http: HttpClient, private constant: Constant) {}


  CustomStore() {
    return new CustomStore({
      key: 'menuId',
      load: () => this.constant.sendRequest(this.constant.aokijiApi + 'menu/get'),
      insert: (values) => this.constant.sendRequest(this.constant.aokijiApi + 'menu/post', 'POST', {
        values: JSON.stringify(values)
      }),
      update: (key, values) => this.constant.sendRequest(this.constant.aokijiApi + 'menu/put' , 'PUT', {
        key,
        values: JSON.stringify(values)
      }),
      remove: (key) => this.constant.sendRequest(this.constant.aokijiApi + 'menu/delete', 'DELETE', {
        key
      }),
    });
  }
}
