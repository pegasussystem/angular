import {Subject} from 'rxjs';
import {MenuModel} from '../models/menu.model';
import {Injectable} from '@angular/core';


@Injectable({providedIn: 'root'})
export class AppConfigService {

  //
  // judul dari setiap menu
  menuTitle = new Subject<string>();
  setMenuTitle(title: string) {
    this.menuTitle.next(title);
  }
}
