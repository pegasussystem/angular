import {Subject} from 'rxjs';
import {MenuModel} from '../models/menu.model';
import {Injectable} from '@angular/core';
import {AccountProfileModel} from '../models/account-profile.model';
import {HttpClient} from "@angular/common/http";
import {Constant} from "../shared/constant";


@Injectable({providedIn: 'root'})
export class AccountProfileService {

  //
  // variable
  accountProfile = new Subject<AccountProfileModel>();
  menus = new Subject<MenuModel[]>();


  constructor(private constant: Constant, private http: HttpClient) {}

  setAccountProfile(accountProfileModel: AccountProfileModel) {
    this.accountProfile.next(accountProfileModel);
  }


  fetchPosts() {
    return this.http
      .get<{ [key: number]: MenuModel }>(
        'http://localhost:5000/menu/get-tree'
      );
  }

}
