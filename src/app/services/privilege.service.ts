import {PrivilegeModel} from '../models/privilege.model';
import {Injectable} from '@angular/core';
import {PrivilegeMenuModel} from "../models/privilege-menu.model";


@Injectable({
  providedIn: 'root',
})
export class PrivilegeService {
  privilegeData: PrivilegeModel[] = [
    new PrivilegeModel(1, 'administrator'),
    new PrivilegeModel(2, 'manager'),
    new PrivilegeModel(3, 'penginput'),
  ];

  privilegeMenuData: PrivilegeMenuModel[] = [
    new PrivilegeMenuModel(1, 1, 'manifest', 'create, read, update, delete'),
    new PrivilegeMenuModel(2, 1, 'spb', 'create, read'),
    new PrivilegeMenuModel(3, 1, 'menu 1', 'read'),
    new PrivilegeMenuModel(4, 1, 'menu 2', 'create, read'),
    new PrivilegeMenuModel(5, 1, 'menu 3', 'create, read, approved, unapproved'),
  ];

  gets() {
    return this.privilegeData.slice();
  }

  get(idPrivilege: number) {
    return this.privilegeData[idPrivilege];
  }

  getsPrivilegeMenu() {
    return this.privilegeMenuData.slice();
  }


}
