import {Component, Input, OnInit} from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component implements OnInit {
  cardActionCollapse;
  $cardCollapsedClass = null;
  clicked = false;
  @Input() id;
  @Input() title;
  testvar = 2;


  constructor() { }

  ngOnInit() {

    // if(this.$cardCollapsedClass === null) {
    //   this.cardActionCollapse = function() {
      this.$cardCollapsedClass = $('.card-collapsed');

      // Hide if collapsed by default
      this.$cardCollapsedClass.children('.card-header').nextAll().hide();

      // Rotate icon if collapsed by default
      this.$cardCollapsedClass.find('[data-action=collapse]').addClass('rotate-180');

      // Collapse on click
      $('#' + this.id.toString() + ' [data-action=collapse]:not(.disabled)').on('click', function (e) {
        var $target = $(this),
          slidingSpeed = 150;

        e.preventDefault();
        $target.parents('.card').toggleClass('card-collapsed');
        $target.toggleClass('rotate-180');
        $target.closest('.card').children('.card-header').nextAll().slideToggle(slidingSpeed);
      });
      // };
    // }


  }


}
