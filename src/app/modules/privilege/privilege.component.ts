import { Component, OnInit } from '@angular/core';

import {PrivilegeService} from '../../services/privilege.service';
import {PrivilegeModel} from '../../models/privilege.model';
import {PrivilegeMenuModel} from '../../models/privilege-menu.model';
import {Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";
import {AppConfigService} from "../../services/app-config.service";

@Component({
  selector: 'app-privilege',
  templateUrl: './privilege.component.html',
  styleUrls: ['./privilege.component.css']
})
export class PrivilegeComponent implements OnInit {
  dataSource: PrivilegeModel[];
  privilegesMenuDs: PrivilegeMenuModel[];

  constructor(private privilegeService: PrivilegeService, private router: Router, private appConfig: AppConfigService) {
    this.dataSource = privilegeService.gets();
    this.privilegesMenuDs = privilegeService.getsPrivilegeMenu();

    appConfig.setMenuTitle('Privilege');
  }

  ngOnInit() {
    console.log(this.privilegesMenuDs);
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'plus',
          type: 'default',
          stylingMode: 'contained',
          onClick: this.showForm.bind(this)
        }
      }
    );
  }

  showForm(e) {
    this.router.navigate(['/privilege/create']);
  }

  test() {
    this.dataSource = null;
  }

}
