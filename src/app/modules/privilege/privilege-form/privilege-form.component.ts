import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-privilege-form',
  templateUrl: './privilege-form.component.html',
  styleUrls: ['./privilege-form.component.css']
})
export class PrivilegeFormComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  privilegeForm: FormGroup;
  updateMode = false;

  actions: { value: string; key: string }[] = [
    {key: 'c', value: 'Create'},
    {key: 'r', value: 'Read'},
    {key: 'u', value: 'Update'},
    {key: 'd', value: 'delete'},
    ];

  menus: { value: string; key: string }[] = [
    {key: '10', value: 'Menu 1'},
    {key: '20', value: 'Menu 2'},
    {key: '30', value: 'Menu 33333'},
  ];

  privileges = {
    '10_c': true, '1_r': false, '1_u': false, '1_d': false,
    '20_c': false, '2_r': true, '2_u': false, '2_d': false,
    '30_c': false, '3_r': false, '3_u': true, '3_d': false,
    '40_c': false, '4_r': false, '4_u': false, '4_d': true
  };

  constructor(private fb: FormBuilder) { }

  ngOnInit() {

    let privRow = {};
    let rowKey = '';

    this.dtOptions = {
      paging: false, searching: false,
      ordering: false
    };

    this.privilegeForm = this.fb.group({
      privilegeName: [''],
      privileges: this.fb.array([ ])
    });

    // isi tabel
    for (const row1 in this.menus) {
      privRow = {};
      if (!this.menus.hasOwnProperty(row1)) { continue; }
      for (const row2 in this.actions) {
        if (!this.actions.hasOwnProperty(row2)) { continue; }
        rowKey = this.menus[row1].key + '_' + this.actions[row2].key;
        privRow[rowKey] = (this.updateMode) ? this.privileges[rowKey] : false ;
      }
      // console.log(this.privilegeForm.get('privilege'));
      (this.privilegeForm.get('privileges') as FormArray).push(this.fb.group(privRow));
    }






  }

  // createItem(): FormGroup {
  //   // const row = { menu1c: [''], menu1r: [''], menu1u: [''], };
  //   // row.menu1d = [''];
  //   // return this.fb.group(row);
  // }

  // onFormSubmit = function(e) {
  //   console.log(this.privilegeForm.value);
  //   e.preventDefault();
  // }

  onFormSubmit(e) {
    // ### transform privilegeForm sebelum dikirim ke API

    let privValue = '';
    let privList = [];
    let privId = '';
    let rtn = [];

    for (const row1 in this.privilegeForm.value.privileges) {
      const data1 = this.privilegeForm.value.privileges[row1];
      for (const row2 in data1) {
        if (data1[row2] === true) {
          privValue = privValue + row2.split('_')[1] + ',';
          privId = 'menuId_' + row2.split('_')[0];
        }
      }
      if (privId !== '')  {privList[privId] = privValue;}
      privValue = '';
      privId = '';
    }

    rtn['privilegeName'] = this.privilegeForm.value.privilegeName;
    rtn['privileges'] = privList;

    console.log(rtn);

    e.preventDefault();
  }

  test() {
    // console.log(this.privilegeForm);
    // console.log(this.privilegeForm2);

    // filter
    // const priv = this.privilegeForm2.filter(x => x.key === 'privilege');
    // console.log(priv);
    // console.log(priv[0]);
    // const test = priv.filter(x => x.key === 10);
    // console.log(test);

  }

}
