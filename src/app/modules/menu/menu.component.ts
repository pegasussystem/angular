import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import CustomStore from 'devextreme/data/custom_store';
import { HttpClient, HttpClientModule, HttpHeaders, HttpParams } from '@angular/common/http';

import {AppConfigService} from '../../services/app-config.service';
import {MenuService} from '../../services/menu.service';
import { MenuModel } from '../../models/menu.model';
import {Constant} from '../../shared/constant';
// import {event} from 'devextreme/bundles/dx.all';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy {
  dsMenu: any;
  menus: MenuModel[];
  isPage: boolean;
  storeMenuList: CustomStore;
  menuSubscription: Subscription;
  // menuTitle: string;
  menuTitleSub: Subscription;

  constructor(private menuService: MenuService, private http: HttpClient, private appConfig: AppConfigService, private constant: Constant) {

    //
    // set variable
    this.isPage = false;
    this.dsMenu = menuService.CustomStore();

    // - menu title
    // this.menuTitleSub = this.appConfig.menuTitle
    //   .subscribe( (value: string) => {
    //     this.menuTitle = value;
    //   });
    appConfig.setMenuTitle('Menu');


    // this.dsMenu = new CustomStore({
    //   key: 'menuId',
    //   load: () => this.sendRequest('http://localhost:5000/menu/api/get'),
    //   insert: (values) => this.sendRequest('http://localhost:5000/menu/api/post', 'POST', {
    //     values: JSON.stringify(values)
    //   }),
    //   update: (key, values) => this.sendRequest('http://localhost:5000/menu/api/put', 'PUT', {
    //     key,
    //     values: JSON.stringify(values)
    //   }),
    //   remove: (key) => this.sendRequest('http://localhost:5000/menu/api/delete', 'DELETE', {
    //     key
    //   })
    // });


    this.storeMenuList = new CustomStore({
      key: 'menuId',
      load: () => this.constant.sendRequest(this.constant.aokijiApi + 'menu/get'),
      byKey: (key) => {
        return this.http.get(this.constant.aokijiApi + 'menu/get')
          .toPromise();
      }
    });


  }

  ngOnInit() {

    //
    // subs menu data
    // this.menuSubscription = this.menuService.menus
    //   .subscribe( (value: MenuModel[]) => {
    //     this.menus = value;
    //   });

    //
    // get

  }


  onClick() {
    // this.menuService.getData();
  }

  testClick(e) {
    this.isPage = !this.isPage;
    // this.isPage = e.value;
    // console.log(e.value);
    console.log(this.isPage);

  }

  ngOnDestroy() {
    // this.menuSubscription.unsubscribe();
  }


  onRowUpdating(options) {
    options.newData = $.extend({}, options.oldData, options.newData);
  }

  // sendRequest(url: string, method: string = 'GET', data: any = {}): any {
  //
  //   const httpParams = new HttpParams({ fromObject: data });
  //   const httpOptions = { withCredentials: false, body: httpParams };
  //   let result;
  //
  //   switch (method) {
  //     case 'GET':
  //       result = this.http.get(url, httpOptions);
  //       break;
  //     case 'PUT':
  //       result = this.http.put(url, httpParams, httpOptions);
  //       break;
  //     case 'POST':
  //       result = this.http.post(url, httpParams, httpOptions);
  //       break;
  //     case 'DELETE':
  //       result = this.http.delete(url, httpOptions);
  //       break;
  //   }
  //
  //   return result
  //     .toPromise()
  //     .then((data: any) => {
  //       return method === 'GET' ? data : data;
  //     })
  //     .catch(e => {
  //       throw e && e.error && e.error.Message;
  //     });
  // }


}
