import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

// import {DevExtremeModule, DxButtonModule} from 'devextreme-angular';
import { DxDataGridModule } from 'devextreme-angular/ui/data-grid';
import { DxButtonModule } from 'devextreme-angular/ui/button';
import { DxFormModule } from 'devextreme-angular/ui/form';
import { DxTextAreaModule } from 'devextreme-angular/ui/text-area';
import { DxTextBoxModule } from 'devextreme-angular/ui/text-box';
import { DxCheckBoxModule } from 'devextreme-angular/ui/check-box';
import { DxSwitchModule } from 'devextreme-angular/ui/switch';
import { DxValidatorModule } from 'devextreme-angular/ui/validator';
import { DxValidationSummaryModule } from 'devextreme-angular/ui/validation-summary';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Test1Component } from './test/test1/test1.component';
import { Test2Component } from './test/test2/test2.component';
import { CardComponent } from './components/card/card.component';
import { PrivilegeComponent } from './modules/privilege/privilege.component';
import { DataGridComponent } from './components/data-grid/data-grid.component';
import { PrivilegeFormComponent } from './modules/privilege/privilege-form/privilege-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import { MenuComponent } from './modules/menu/menu.component';
import {HttpInterceptorService} from './shared/http-interceptor.service';
import { Constant } from './shared/constant';

@NgModule({
  declarations: [
    AppComponent,
    Test1Component,
    Test2Component,
    CardComponent,
    PrivilegeComponent,
    DataGridComponent,
    PrivilegeFormComponent,
    MenuComponent
  ],
  imports: [
    FormsModule, HttpClientModule,
    BrowserModule,
    AppRoutingModule,

    DataTablesModule,

    DxButtonModule, DxDataGridModule, DxFormModule, DxTextAreaModule, DxTextBoxModule, DxSwitchModule, ReactiveFormsModule,
    DxValidatorModule, DxValidationSummaryModule, DxCheckBoxModule
  ],
  providers: [
    Constant,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
