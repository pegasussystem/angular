declare var $: any;
import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  cardCollapsedClass = null;
  @Input() id;
  @Input() title;

  constructor() { }

  ngOnInit() {
    this.cardCollapsedClass = $('.card-collapsed');

    // Hide if collapsed by default
    this.cardCollapsedClass.children('.card-header').nextAll().hide();

    // Rotate icon if collapsed by default
    this.cardCollapsedClass.find('[data-action=collapse]').addClass('rotate-180');

    // Collapse on click
    $('#' + this.id.toString() + ' [data-action=collapse]:not(.disabled)').on('click', function (e) {
      var $target = $(this),
        slidingSpeed = 150;

      e.preventDefault();
      $target.parents('.card').toggleClass('card-collapsed');
      $target.toggleClass('rotate-180');
      $target.closest('.card').children('.card-header').nextAll().slideToggle(slidingSpeed);
    });
  }

}
