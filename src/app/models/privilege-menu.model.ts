export class PrivilegeMenuModel {
  constructor(public idPrivilegeMenu: number,
              public idPrivilege: number,
              public menuName: string,
              public action: string
  ) {}

}
