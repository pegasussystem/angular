export class AccountProfileModel {
  constructor(public accountId: number,
              public accountName: string,
              public branchId: number,
              public branchName: number,
  ) {}

}
