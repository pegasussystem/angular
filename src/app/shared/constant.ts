import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable()
export class Constant {
  constructor(private http: HttpClient) {}

  readonly aokijiApi: string = 'http://localhost:5000/';

  readonly distLocation: string = 'MyApplication/';

  sendRequest(url: string, method: string = 'GET', data: any = {}): any {
    const httpParams = new HttpParams({ fromObject: data });
    const httpOptions = { withCredentials: false, body: httpParams };
    let result;

    switch (method) {
      case 'GET':
        result = this.http.get(url, httpOptions);
        break;
      case 'PUT':
        result = this.http.put(url, httpParams, httpOptions);
        break;
      case 'POST':
        result = this.http.post(url, httpParams, httpOptions);
        break;
      case 'DELETE':
        result = this.http.delete(url, httpOptions);
        break;
    }

    return result
      .toPromise()
      // tslint:disable-next-line:no-shadowed-variable
      .then((data: any) => {
        return method === 'GET' ? data : data;
      })
      .catch(e => {
        if (e.status === 422) { throw new Error(e.error.data); }
        throw e.message;
        // throw e && e.error && e.error.Message;
      });
  }
}
