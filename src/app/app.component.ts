// HOW TO
// build to pord ==> ng build --prod
// menjalankan setelah build prod ==> lite-server --baseDir="dist/angular"

import {Component, OnDestroy} from '@angular/core';
import DataGrid from 'devextreme/ui/data_grid';
import {HttpClient} from '@angular/common/http';
import {AppConfigService} from './services/app-config.service';
import {Subscription} from 'rxjs';
import {AccountProfileService} from './services/account-profile.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'angular';

  menuTitle: string;
  menuTitleSub: Subscription;

  userMenu: any;


  constructor(private appConfig: AppConfigService, private accountProfileService: AccountProfileService) {

    //
    // SET VARIABLE

    // - title
    this.menuTitleSub = this.appConfig.menuTitle
      .subscribe( (value: string) => {
        this.menuTitle = value;
      });

    DataGrid.defaultOptions({
      options: {
        showRowLines: true,
        columnAutoWidth: true,
        showColumnLines: true,
        rowAlternationEnabled: true,
        allowColumnResizing: true,
        showBorders: true
      }
    });

    //
    // get data
    //-- menu tree
    this.accountProfileService.fetchPosts().subscribe(
      data => {
        this.userMenu = data;
        console.log(data);
      }
    );
  }

  ngOnDestroy() {
    this.menuTitleSub.unsubscribe();
  }

}
